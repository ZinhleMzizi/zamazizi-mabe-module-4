// ignore_for_file: use_key_in_widget_constructors, prefer_typing_uninitialized_variables, non_constant_identifier_names

import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

	
// This widget is the root of your application.
@override
Widget build(BuildContext context) {
	var splashTransition;
	return MaterialApp(
		
	// Application name
	title: 'Whats in your fridge Food App',
		
	// Application theme data, you can set the
	// colors for the application as
	// you want
	theme: ThemeData(
		primarySwatch: Colors.green,
	),
		
	// A widget which will be started on application startup
	home: AnimatedSplashScreen(
		splash: 'images/unnamed.png',
		nextScreen: const MyHomePage(title: 'Whats in your fridge'),
		splashTransition: splashTransition.fadeTransition,
	),
	);
}

  AnimatedSplashScreen({required String splash, required MyHomePage nextScreen, splashTransition}) {}
}

class MyHomePage extends StatelessWidget {
final String title;

const MyHomePage({required this.title});

@override
Widget build(BuildContext context) {
	return Scaffold(
	appBar: AppBar(
		
		// The title text which will be shown on the action bar
		title: Text(title),
	),
	body: const Center(
		child: Text(
		'Whats in your fridge!',
		),
	),
	);
}
}
